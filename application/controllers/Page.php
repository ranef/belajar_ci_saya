<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index()
	{
		$this->load->view('beranda');
	}
	public function about()
	{
		$this->load->view('about');
	}
	public function contact()
	{
		$this->load->view('contact');
	}
	public function ganjil()
	{
		$this->load->view('ganjil');
	}
	public function genap()
	{
		$this->load->view('genap');
	}
}
