<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Angka extends CI_Controller {

	public function index()
	{
		$this->load->view('beranda');
	}
	public function ganjil()
	{
		$this->load->view('ganjil');
	}
	public function genap()
	{
		$this->load->view('genap');
	}
}
